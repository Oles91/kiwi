const body = document.getElementById('body');

const openPopUp = document.getElementById('btn-Sign-up');

const closePopUp = document.getElementById('pop_up-close');

const popUp = document.querySelector('.pop_up')

openPopUp.addEventListener('click', function (e) {
    e.preventDefault();
    popUp.classList.add('active');
    body.classList.add('lock');
})

closePopUp.addEventListener('click', function (e) {
    e.preventDefault();
    popUp.classList.remove('active');
    body.classList.remove('lock');
})

// mobile__menu-burger
const openMenuBurger = document.getElementById('mobile__menu-burger');

const menuBurger = document.getElementById('menu__burger');

const closeMenuBurger = document.getElementById('menu__burger-close');

openMenuBurger.addEventListener('click', function (e) {
    e.preventDefault();
    menuBurger.classList.add('active');
    openMenuBurger.classList.add('hidden');
    body.classList.add('lock');
})

closeMenuBurger.addEventListener('click' , function (e) {
    e.preventDefault();
    menuBurger.classList.remove('active');
    openMenuBurger.classList.remove('hidden');
    body.classList.remove('lock');
})

